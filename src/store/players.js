import { defineStore } from 'pinia';
import { ref } from 'vue';
import { assignAllRoles } from '@/lib/role-assign';

export const usePlayersStore = defineStore('players', {
  state: () => ({
    players: ref([]),
  }),
  actions: {
    addPlayer(name) {
      this.players.push({
        name,
      })
    },
    assignRoles() {
      const rolesIds = assignAllRoles(this.players.length);
      this.players.forEach((player, index) => {
        player.roleId = rolesIds[index];
      });
    }
  },
});
