import roles from './cards.json';
import { shuffle, difference } from 'lodash';

const getARole = (teamName, chosenRoleIds) => {
  const roleIdsInTeam = roles.filter(role => role.team === teamName).map(({ id }) => id);
  const chooseableRoleIds = difference(roleIdsInTeam, chosenRoleIds);

  return shuffle(chooseableRoleIds)[0];
}

export const assignAllRoles = (numberOfPlayers) => {
  const roleIds = [
    0, // President
    1, // Bomber
  ]

  const isOddNumberOfPlayers = numberOfPlayers % 2 === 1;

  if (isOddNumberOfPlayers) {
    roleIds.push(getARole("grey", roleIds)); // Neutral
  }

  for (let i = 0; i < numberOfPlayers / 2 - 1; i++) {
    roleIds.push(getARole("blue", roleIds));
    roleIds.push(getARole("red", roleIds));
  }

  return roleIds;
}